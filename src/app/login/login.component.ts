import { ResponseUsuario } from './response.usuario.model';
import { NotificationService } from './../snackbar/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'tdv-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  navigateTo: string

  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private notificationService: NotificationService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      codigo: this.fb.control('',[Validators.required])
    })
    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/home')
    this.loginService.handleLogout()
  }

  mensagem(responseUsuario: ResponseUsuario): string{
    if(responseUsuario.codigo == 1){
      this.router.navigate([atob(this.navigateTo)])
      return `Bem vindo, ${responseUsuario.usuario.nome}`
    }else{
      return `Usuario não encontrado`
    }
  }

  login(){
    this.loginService.login(this.loginForm.value.codigo)
                          .subscribe(responseUsuario => 
                                      this.notificationService.notify(this.mensagem(responseUsuario)),
                                     response =>
                                     this.notificationService.notify(response.error.message))
  }
}