import { Usuario } from './../tabs/create-os/usuario.model';
export class ResponseUsuario{
    
    constructor(public codigo: number,
                public mensagem: string,
                public usuario: Usuario ){
    }
}