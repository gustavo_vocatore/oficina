import { ResponseUsuario } from './response.usuario.model';
import { MEAT_API } from './../app.api';
import { Usuario } from './../tabs/create-os/usuario.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router, NavigationEnd } from '@angular/router'
import { Observable } from 'rxjs'
import { tap, filter } from 'rxjs/operators'

@Injectable()
export class LoginService{

    responseUsuario: ResponseUsuario
    lastUrl: string

    constructor(private http: HttpClient, private router: Router){
        this.router.events.pipe(filter(e => e instanceof NavigationEnd)).
                          subscribe((e: NavigationEnd) => this.lastUrl = e.url)
    }

    isLoggedIn():boolean{
        if(this.responseUsuario !== undefined && this.responseUsuario.codigo == 1){
            return true
        }
        return false
    }

    login(codigo: string): Observable<ResponseUsuario>{
        return this.http.get<ResponseUsuario>(`${MEAT_API}/login/${codigo}`)
                                    .pipe(tap(responseUsuario => this.responseUsuario = responseUsuario))
    }

    handleLogout(){
        this.responseUsuario = undefined
    }

    handleLogin(path: string = this.lastUrl){
        this.router.navigate(['/login'])
    }
}