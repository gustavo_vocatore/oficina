import { CreateOsService } from './tabs/create-os/create-os.service';
import { AuthInterceptor } from './login/auth.interceptor';
import { LoggedinGuard } from './login/loggedin.guard';
import { AplicationErrorHandler } from './app-error-handler';
import { LoginService } from './login/login.service';
import { NotificationService } from './snackbar/notification.service';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RadioComponent } from './shared/radio/radio.component';
import { InputComponent } from './shared/input/input.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Input, ErrorHandler } from '@angular/core';
import { ROUTES } from './app.routes';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { TabCreateComponent } from './tabs/tab-create/tab-create.component';
import { CreateOsComponent } from './tabs/create-os/create-os.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    SnackbarComponent,
    TabCreateComponent,
    CreateOsComponent,
    InputComponent,
    RadioComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [LoginService,
              CreateOsService,
              FormBuilder,             
              CommonModule,
              FormsModule,
              ReactiveFormsModule,
              LoggedinGuard,
              NotificationService,
              {provide: ErrorHandler, useClass: AplicationErrorHandler },
              {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
