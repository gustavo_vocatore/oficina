import { LoggedinGuard } from './login/loggedin.guard';
import { LoginComponent } from './login/login.component';
import { Routes } from '@angular/router'
import { HomeComponent } from './home/home.component'
import { TabCreateComponent } from './tabs/tab-create/tab-create.component';

export const ROUTES: Routes =[
    {path: '', component: LoginComponent},
    {path: 'login/:to', component: LoginComponent},
    {path: 'login', component: LoginComponent},
    {path: 'home', component: HomeComponent , canActivate: [LoggedinGuard]},
    {path: 'cadastrar',component: TabCreateComponent, canActivate: [LoggedinGuard]}
]