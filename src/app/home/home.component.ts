import { Usuario } from './../tabs/create-os/usuario.model';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tdv-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  usuario: Usuario

  constructor(private route: ActivatedRoute ) { }

  ngOnInit() {
    this.route.params.subscribe();
  }
}