import { Usuario } from './usuario.model';
export class PreOs{

    constructor(public codigo: number,
                public capacidade: string,
                public tipoManutencao: string,
                public frota: string,
                public carreta: string,
                public km: number,
                public local: string,
                public dataAberturaOs: Date,
                public dataFechamentoOS: Date,
                public dataPrevisao: Date,
                public dataCadastro: Date,
                public usuario: Usuario){
    }
} 