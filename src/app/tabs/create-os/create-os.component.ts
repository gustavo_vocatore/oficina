import { tap } from 'rxjs/operators';
import { CreateOsService } from './create-os.service';
import { ResponseUsuario } from './../../login/response.usuario.model';
import { LoginService } from './../../login/login.service';
import { PreOs } from './preos.model';
import { RadioOption } from './../../shared/radio/radio-option.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from './usuario.model';
import { ResponseModel } from './response.model';

@Component({
  selector: 'tdv-crete-os',
  templateUrl: './create-os.component.html',
  styleUrls: ['./create-os.component.css']
})
export class CreateOsComponent implements OnInit {

  cadastroForm: FormGroup
  usuario: Usuario
  numberPattern = /^[0-9]*$/
  responseModel: ResponseModel

  capacidades: RadioOption[] =[
    {label: 'Carregado', value: 'CARREGADO'},
    {label: 'Cheio', value: 'CHEIO'}
  ]
  
  tipoManutencoes: RadioOption[] =[
    {label: 'Corretiva', value: 'CORRETIVA'},
    {label: 'Preventiva', value: 'PREVENTIVA'}
  ]

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private loginService: LoginService,
              private createOsService: CreateOsService) { }

  ngOnInit() {
  this.cadastroForm = this.formBuilder.group({
    capacidade: this.formBuilder.control(''),
    tipoManutencao: this.formBuilder.control(''),
    frota: this.formBuilder.control(''),
    carreta: this.formBuilder.control(''),
    km: this.formBuilder.control('',[Validators.pattern(this.numberPattern)]),
    dataPrevisao: this.formBuilder.control(''),
    dataAberturaOs: this.formBuilder.control(''),
    dataFechamentoOs: this.formBuilder.control(''),
    local: this.formBuilder.control('')
    })
    this.usuario = this.loginService.responseUsuario.usuario
  }

  cadastrarOs(preOs: PreOs){
    preOs.usuario = this.usuario
    this.createOsService.savePreOs(preOs)
      .pipe(tap((responseModel: ResponseModel) => {
        this.responseModel = responseModel
      }))
  }
}