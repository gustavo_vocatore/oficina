import { ResponseModel } from './response.model';
import { MEAT_API } from './../../app.api';
import { PreOs } from './preos.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router, NavigationEnd } from '@angular/router'
import { Observable } from 'rxjs'
import { tap, filter, map } from 'rxjs/operators'

@Injectable()
export class CreateOsService{
    
    constructor(private http: HttpClient, private router: Router){
    }

    savePreOs(preOs: PreOs): Observable<ResponseModel>{
        return this.http.post<ResponseModel>(`${MEAT_API}/preos/save`,preOs)
                                        .pipe(map(preOs => preOs)) 
    }  
}