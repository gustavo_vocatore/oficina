import { LoginService } from './../login/login.service';
import { Usuario } from './../tabs/create-os/usuario.model';
import { Component, OnInit } from '@angular/core';
import { ResponseUsuario } from '../login/response.usuario.model';

@Component({
  selector: 'tdv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }

  user(): ResponseUsuario{
    return this.loginService.responseUsuario
  }

  isLoggedIn(): boolean{
    return this.loginService.isLoggedIn()
  }

  login(){
    this.loginService.handleLogin()
  }

  logout(){
    this.loginService.handleLogout()
  }
}